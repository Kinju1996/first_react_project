import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
class Addlist extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      textvalue: '',
      listarray: [],
      updatelistindex: '',
    }
  
  }
  /** 
   * 
   * @param {*} index     //particular delete element index
   * @param {*} e         //pass this
   */
  updateValue(index, e) {
    var indexvalue = this.state.listarray[index];
    this.setState({textvalue: indexvalue, updatelistindex: index})
  }
  /**
   * Delete the element  
   */
  handleDelete(index) {
      this
      .state
      .listarray
      .splice(index, 1);
    this.setState({listarray: this.state.listarray})
  }

  /**
   * this method set the value is state
   * @param {*} e  
   */
  addvalues(e) {
    this.setState({textvalue: e.target.value})
  }

  /**
   *  this method is use for submit the value
   * @param {*} e 
   */
  onSubmit(e) {
    // this.validation(this.state.textvalue)
    e.preventDefault();
    var index = this.state.updatelistindex;
    var value = this.state.textvalue;

    // if  update the value its going to if condition  and I add the value its going on else condition
   if(this.state.textvalue == ''){
     alert("please Enter the value")
   }else{
    if (index !== '' ) { 
      const listarray = this.state.listarray
      listarray[index] = this.state.textvalue;
      this.setState({
        listarray,
        textvalue: '',
      });

    } else {                     
      this.setState({
        textvalue: '',
        listarray: [
          ...this.state.listarray,
          this.state.textvalue
        ]
      });
    }
   }
 }
  render() {
    return (
      <div className="main-parent">
        <form
          className="myform"
          onSubmit={this
          .onSubmit
          .bind(this)}>
          <div className="form-row inner-div">
            <input
              type="text"
              name="mytext"
              className="mytext"
              id="mytext"
              value={this.state.textvalue}
              onChange={this
              .addvalues
              .bind(this)}/>
            <button className="add-button btn">
              Add
            </button>
          </div>
        </form>

        <div className="list-parent">
          <h3>
            List
          </h3>
          <ul className="list">

            {this
              .state
              .listarray
              .map((list, index) => <li key={index} id={`list-${index}`}>

                <div className="text">
                  <p>
                    <span>
                      {list}</span>
                    <input
                      type="button"
                      value="delete"
                      className="delete-button btn"
                      onClick={this
                      .handleDelete
                      .bind(this, index)}/>
                    <input
                      type="button"
                      value="update"
                      className="update-button btn"
                      onClick={this
                      .updateValue
                      .bind(this, index)}/>

                  </p>
                </div>
              </li>)}
          </ul>

        </div>

      </div>
    )
  }
}
ReactDOM.render(
  <Addlist/>, document.getElementById('root'));
